// Adresse où récupérer le JSON
//     http://www.lesonunique.com/appli/now.json


// On prépare la requête
var request = new XMLHttpRequest();
request.open('GET', 'http://www.lesonunique.com/appli/now.json');

// Action une fois la requête complétée (asynchrone)
function traitement() {
  if (request.status >= 200 && request.status < 400) {
  alert('Success !');
  } else {
  alert('erreur HTTP : ' + request.status);
  }
}

request.onload = function() {
var data = JSON.parse(request.responseText);
console.log(data);

var artiste = data.current.artiste;
var chanson = data.current.title;
var image = data.current.cover_url;

// Ici mettre dans le HTML l'artiste et la chanson
var artiste_html = document.querySelector("#chanteur");
var chanson_html = document.querySelector("#titre");
var image_html = document.querySelector("img");

artiste_html.textContent = 'Artiste : '+ artiste;
chanson_html.textContent = 'Chanson : '+ chanson;
image_html.src = image;
};

// Lancement de la requête
request.onerror = function () {
  // Ici mettre dans le HTML l'artiste et la chanson
  var artiste_html = document.querySelector("#cote");
  var chanson_html = document.querySelector("#cote");
  var image_html = document.querySelector("img");

  artiste_html = 'inconnu';
  chanson_html = 'inconnue';
image_html = 'http://www.safewatercube.com/wp-content/uploads/2016/10/Sun-Radio.png';
};
request.send();
