TD 2 Javascript - AJAX
-----------------------

Conseils généraux :

- Utilisez la « console web » du navigateur pour voir ce qui se passe

1. SUN radio
-------------

But : créer une page HTML affichant ce qui passe sur la radio **SUN Nantes**
(titre, description et image).

**(pour cette question 1, il est nécessaire de désactiver la protection CORS, cf cours, et conseillé d'avoir l'extension JSONView installée)**

### Préparer le terrain

Faire une page basique HTML + CSS qui présentera :
- le titre de la Chanson (Afficher « Titre inconnu » pour l'instant)
- l'artiste (Afficher « Artiste inconnu » pour l'instant),
- une image (mettre une image par défaut de votre choix, en attendant de charger une « vraie » image)

### Charger les données

Utiliser JavaScript avec AJAX pour

  - Récupérer l'information au format JSON à l'adresse
    http://www.lesonunique.com/appli/now.json
  - l'injecter dans votre page HTML (titre, description)

*NB: vous devez fouiller un peu dans le JSON renvoyé pour trouver le bon champ
à afficher*

### Rester à jour

Faire en sorte que l'information soit rafraîchie toutes les 30s (indice:
  `window.setInterval`).

2. MorAPIon
------------

Une API en ligne est fournie, elle héberge des parties du célèbre jeu du
Morpion. Sa documentation est fournie à la fin de cette page.

Une fois implémenté, vous devez pouvoir jouer en réseau contre un autre
stagiaire qui utilise sa propre interface (puisque vous exploitez la même
API).

La logique de jeu est gérée côté serveur, il n'y a donc pas **pas** besoin de
faire les choses suivantes en JavaScript :

- savoir si la partie est terminée
- savoir si un coup est valide
- savoir qui a gagné

Implémenter le jeu de morpion utilisant l'API à http://morapion.somuch.lol

Étapes
------

Quelques étapes sont donnés pour guider l'exercice :

### a. Récupérer la liste des parties existantes

Insérer, *via* JavaScript, dans le HTML la liste des parties ; distinguer les
parties en cours des parties terminées.

*Suggestion de présentation : via un `<select>` :*

![un <select> avec des <optgroup>](figs/select.png)

### b. Afficher une partie

Faire en sorte que lorsque l'utilisateur choisit une partie dans la liste
(évènement `change`), cela l'affiche à l'utilisateur (utiliser une `<table>`
HTML, et la remplir avec le contenu récupéré en AJAX).

Afficher également les autres infos (état, joueur courant, vainqueur).

### c. Créer une nouvelle partie

Offrir un formulaire demandant :

- identifiant (nom) de la partie
- mot de passe de la partie

Conseils :
- Traiter les éventuelles erreurs
- vérifier que la nouvelle partie s'affiche lorsque vous chargez la liste des
parties).

### d. Rejoindre une partie (déjà existante sur le serveur)

Quand une partie est affichée ; offrir à l'utilisateur de la rejoindre en tant
que joueur. pour cela, il devra remplir un formulaire contenant les deux
champs :

- mot de passe de la partie
en cliquant sur son
- joueur à incarner (*x* ou *o*), par exemple via un`<select>`
nom dans la liste ;

Si le joueur valide le formulaire (ex: via un bouton *Rejoindre*):

- on masque le formulaire pour que le joueur ne puisse pas le changer en cours
de partie).


### e. Jouer un coup

En cliquant sur une case du tableau, on déclenche une requête AJAX vers le
serveur pour jouer un coup.

- traiter les éventuelles erreurs retournées par le serveur
- mettre à jour l'affichage de la grille dans le HTML

### f. rafraîchir la partie

Afficher un bouton pour rafraichir la partie (au cas où l'autre joueur ait
joué).

### g. … automatiquement

Faire une requête toutes les 5s pour rafraîchir la partie ;

### Check-list

- Sur chaque requête AJAX, on traite les erreurs HTTP, et on en informe l'utilisateur
- Le JavaScript ne me laisse pas jouer si ça n'est pas son tour
- L'affichage n'est mis à jour que si la requête d'état de partie dit que c'est
à mon tour ou que la partie est terminée.
- Les requêtes de rafraichissement n'ont psa lieu quand c'est à mon tour de
jouer (puisque c'est une action de ma part qui est attendue).
- On gère le cas où l'API nous retourne une liste de parties vide.

### BONUS
Quelques idées :

- Animations visuelles lorsqu'on joue


Documentation de l'API
-----------------------

Cette API héberge des parties de Morpion. Elle gère la logique de jeu et permet
la récupération et la modification de l'état du jeu par les deux joueurs, via
le réseau.

Les parties sont définies par :

- un identifiant, choisi librement (composé de caractères et de tirets)
- un mot de passe, permettant de limiter l'accès

Les joueurs sont nommés par leur symbole : '`x`' et `o`.

Les cellules sont nommées par un système de coordonnées :

        A   B   C
      ╭───┬───┬───┐
    1 │ x │   │   │
      ├───┼───┼───┤
    2 │   │   │   │
      ├───┼───┼───┤
    3 │   │ o │   │
      └───┴───┴───┘

Dans cet exemple la cellule `A1` contient la valeur `'x'` et la cellule `B3`
contient la valeur `'o'`. Les autres contiennent `null`.


### Récupérer la liste des parties

- méthode HTTP : `GET`
- URL : `/games`

Format :

    [
      {
        "identifier": "gentlegame",
        "finished": true,
        "winner": "x",
      },
      {
        "identifier": "evilgame",
        "finished": false,
        "winner": null,
      },
    ]

On peut également ne récupérer que les parties en cours :


- méthode HTTP : `GET`
- URL : `/games?active`

… Ou uniquement les parties terminées

- méthode HTTP : `GET`
- URL : `/games?finished`

### Récupérer l'état d'une partie

Ne nécessite pas de mot de passe.

- méthode HTTP : `GET`
- URL : `/games/:identifier`

*NB: `:identifier` est à remplacer par l'identfiant de la partie.

Format :

    {
      "identifier": "gentlegame",
      "finished": true,
      "turn": "o",
      "winner": "x",
      "cells" : {
        "A1" : "x",
        "A2" : "o",
        "A3" : "o",
        "B1" : "x",
        "B2" : "o",
        "B3" : "o",
        "C1" : "x",
        "C2" : "o",
        "C3" : "o"
      }
    }

### Créer une partie

C'est à vous d'envoyer un contenu lors de la requête POST :

- méthode HTTP : `POST`
- URL : `/games`

Il doit être en JSON au format suivant :

    {
      "identifier": "le-nom-de-ma-partie",
      "password": "mypassword"
    }

Les codes de retour HTTP possibles sont :

- *201* : partie créée et accessible.
- *400* : données soumises invalides
- *500* : erreur serveur

Dans tous les cas, le serveur renvoie un contenu JSON contenant un message
explicatif, par exemple :

    {
      "msg": "Une partie avec ce nom existe déjà"
    }


### Jouer un coup

Nécessite un mot de passe (cf ci-après).

- méthode HTTP: `POST`
- URL : `/games/:identifier/play`

Exemple de JSON à envoyer (pour que le joueur *x* joue en A3) :

    {
      "password": "coucou",
      "player": "x",
      "cell": "A3"
    }

Les codes de retour HTTP possibles sont :

- *200* : Le coup a été enregistré
- *400* : invalide
- *401* : mauvais mot de passe
- *403* : pas au tour de ce joueur de jouer
- *500* : erreur serveur
